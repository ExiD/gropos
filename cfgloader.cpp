#include "cfgloader.h"

cfgLoader::cfgLoader(QString path)
{
    QSettings cfg(path,QSettings::IniFormat);

    path_archive_img       =   cfg.value("path_archive_img").toString();
    path_archive_gps       =   cfg.value("path_archive_gps").toString();
    filaname_archive_gps   =   cfg.value("filaname_archive_gps").toString();
    path_current           =   cfg.value("path_current").toString();

}

