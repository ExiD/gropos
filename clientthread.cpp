#include "clientthread.h"
#include "datacontroller.h"

ClientThread::ClientThread(qintptr ID, QObject *parent) :
    QThread(parent)
{
    this->socketDescriptor = ID;
}

void ClientThread::run() {
    // thread starts here
    socket = new QTcpSocket();

    // set the ID
    if(!socket->setSocketDescriptor(this->socketDescriptor)) {
        // something's wrong, we just emit a signal
        emit error(socket->error());
        return;
    }

    dataController* t_data = new dataController();
    while (socket->state()==QAbstractSocket::ConnectedState) {
        QByteArray* buf = new QByteArray();

        //read header
        if(readyRead(buf, 12) == 12) {
            if(!t_data->checkHeader(*buf)) {
                qDebug() << "ERROR in the header";
            }
        }
        //read image
        if(readyRead(buf, t_data->img_length) == t_data->img_length) {
            if(!t_data->chekImage(*buf)) {
                qDebug() << "ERROR in the image";
            }
        }
        //read gps
        if(readyRead(buf, t_data->gps_length) == t_data->gps_length) {
            if(!t_data->checkGPS(*buf)) {
                qDebug() << "ERROR in the gps";
            }
        }
        //read chksum
        if(readyRead(buf, 2) == 2) {
            if(!t_data->checkChSum(*buf)) {
                qDebug() << "ERROR in the checkSumm";
            }
        }
        delete buf;
        break;
    }
    delete t_data;
    exec();
}

int ClientThread::readyRead(QByteArray* Buffer, int length)
{
    Buffer->clear();
    while(socket->bytesAvailable()<length) {

        if(!socket->waitForReadyRead(10000))
        {
            socket->disconnectFromHost();
            socket->close();
            qDebug() << "Connection is broken from device " << socketDescriptor;
            return -1;
        }
    }
    Buffer->append(socket->read(length));
    return Buffer->length();
}
