#include "server.h"
#include "clientthread.h"

Server::Server(QObject *parent) :
    QTcpServer(parent)
{
}

void Server::startServer(int port) {

    if(!this->listen(QHostAddress::Any,port)) {
        qDebug() << "Could not start server";
    }
    else {
        qDebug() << "Server started succesfully on port " << port;
    }
}

void Server::incomingConnection(qintptr socketDescriptor)
{
    // We have a new connection
    qDebug() << socketDescriptor << " Connecting...";

    ClientThread *thread = new ClientThread(socketDescriptor, this);

    // connect signal/slot
    // once a thread is not needed, it will be beleted later
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

    thread->start();
}
