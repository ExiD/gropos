#ifndef CLIENTTHREAD_H
#define CLIENTTHREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QDebug>

class ClientThread : public QThread
{
    Q_OBJECT
public:
    explicit ClientThread(qintptr ID, QObject *parent = 0);
    int readyRead(QByteArray *Buffer, int length);
    void run();

signals:
    void error(QTcpSocket::SocketError socketerror);
private:
    QTcpSocket *socket;
    qintptr socketDescriptor;
    int conTimeout;
};

#endif // CLIENTTHREAD_H
