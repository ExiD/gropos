#-------------------------------------------------
#
# Project created by QtCreator 2015-11-10T11:28:03
#
#-------------------------------------------------

QT       += core
QT       += network
QT       += gui

TARGET = glanaz
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11

TEMPLATE = app


SOURCES += main.cpp \
    server.cpp \
    clientthread.cpp \
    cfgloader.cpp \
    datacontroller.cpp \
    crc16.cpp

DISTFILES += \

HEADERS += \
    server.h \
    clientthread.h \
    cfgloader.h \
    datacontroller.h \
    crc16.h

