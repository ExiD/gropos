#include "datacontroller.h"

dataController::dataController()
{

}

bool dataController::checkHeader(QByteArray head) {
    //check intro symbols
    for(int i=0;i<3;i++) {
        if (head.at(i) != 0x23) {
            return 0;
        }
        //qDebug() << QString::number ((uchar) head.at(i), 16);
    }
    //read device ID
    device_id = static_cast<quint8>(head.at(3));
    //read image bytesize
    memcpy(&img_length, head.mid(4,4), sizeof(quint32));
    memcpy(&gps_length, head.mid(8,4), sizeof(quint32));
    return 1;
}

bool dataController::chekImage(QByteArray img) {
    if (img.length() == img_length /*&& img.at(0) == 0xFF && img.at(1) == 0xD8*/) {
        image = img;
        QFile outfile("current.jpg");
        QImage m_image;
        m_image.loadFromData(image);
        if (outfile.open(QIODevice::WriteOnly)) {
            m_image.save(&outfile, "JPEG");
            outfile.close();
        }
        return 1;
    }
    return 0;
}

bool dataController::checkGPS(QByteArray gpsc) {
    if (gpsc.length() == gps_length) {
        gps = gpsc;
        return 1;
    }
    return 0;
}

bool dataController::checkChSum(QByteArray chSum) {
    chksum = chSum;
    return 1;
}

bool dataController::checkData() {
    //как делать преобразование этого всего в чар?
}
