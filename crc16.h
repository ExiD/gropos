#ifndef CRC16_H
#define CRC16_H

#include <QtCore/QObject>

using namespace std;

class CRC16 : public QObject
{
    Q_OBJECT
public:
    explicit CRC16(QObject *parent = 0);
    ~CRC16();
    static quint16 getCRC16(const char *nData, int wLength);
    static const ushort wCRCTable[256];

signals:

public slots:
};

#endif // CRC16_H
