#ifndef DATACONTROLLER_H
#define DATACONTROLLER_H

#include <QDebug>
#include <QByteArray>
#include <QString>
#include <QFile>
#include <QImage>

#include "crc16.h"

class dataController
{
public:
    dataController();
    bool checkHeader(QByteArray head);
    bool chekImage(QByteArray img);
    bool checkGPS(QByteArray gpsc);
    bool checkChSum(QByteArray chSum);
    bool checkData();
    quint8 device_id;
    quint32 img_length;
    quint32 gps_length;
    QByteArray image;
    QByteArray gps;
    QByteArray chksum;
};

#endif // DATACONTROLLER_H
