#ifndef CFGLOADER_H
#define CFGLOADER_H

#include <QString>
#include <QSettings>

class cfgLoader
{
public:
    cfgLoader(QString path);

    QString path_archive_img;      //путь к архиву изображений
    QString path_archive_gps;      //путь к архиву GPS коорд
    QString filaname_archive_gps;  //Имя файла-архива (gps.txt)
    QString path_current;          //путь текущих файлов
};

#endif // CFGLOADER_H
