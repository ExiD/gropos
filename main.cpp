#include <QCoreApplication>
#include <QDebug>
#include "server.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    Server server_;
    server_.startServer(1922);

    return app.exec();
}
